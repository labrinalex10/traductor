# traductor

## Requisitos

1. Instalar el programa Translate-shell

## Primeros pasos

Copiamos el archivo a $PATH (Puedes saber la ubicación con echo $PATH)

$ sudo cp gtraductor /usr/local/bin/

## Uso

Ejecutamos escribiendo la palabra gtraductor y escribimos el texto a traducir.

$ gtraductor

## Traducciones

Sólo está configurado para 13 idiomas de todos los idiomas de Google Traductor:

1. Inglés
2. Español
3. Catalán
4. Portugués
5. Italiano
6. Francés
7. Rumano
8. Quechua
9. Aymara
10. Ruso 
11. Alemán
12. Esperanto
13. Turco
